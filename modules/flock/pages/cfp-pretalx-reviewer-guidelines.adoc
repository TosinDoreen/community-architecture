include::ROOT:partial$attributes.adoc[]

= Pretalx CFP Reviewer Guide
Oluwatosin Olatunji; Justin W. Flory

____
Thank you for contributing as a reviewer for Flock to Fedora!
Your expertise is vital in shaping an outstanding conference experience.
This guide is designed to streamline your navigation of the Pretalx review process for Flock.
____

This page provides guidance to call for proposal (CFP) reviewers for the https://flocktofedora.org/[Flock to Fedora] conference.
CFP reviewers are invited by core Flock organizers as volunteers to help curate diverse content and programming at our contributor conference.

[IMPORTANT]
====
Your feedback should be constructive, insightful, and focused on the content of the submissions.
Your role plays a significant part in ensuring a high-quality experience for Flock.
====

This guide is organized into multiple sections below.


[[account]]
== Reviewer account access

You will receive an invitation from a Pretalx administrator with login instructions.
You will be prompted to create a reviewer profile if it is your first time.

.Email invitation from Pretalx
image::pretalx-account-invite.png[Screenshot of the email invitation sent by the Pretalx system to the user's email inbox. The email subject reads, "You have been invited to an organiser team".]

.Accepting the invitation on Pretalx
image::pretalx-invite-acceptance.png[Screenshot of the dialogue to accept the email invitation to join a Pretalx team. Two options are presented to accept, to either sign in with an existing account or create a new one.]


[[proposals]]
== Proposal access

Upon logging in, your dashboard will display a list of proposals assigned to you for review.
Also note that depending on settings, reviewers will be able to view and review all proposals, or only assigned proposals (determined by the Pretalx administrator).

.Reviewer dashboard in Pretalx
image::pretalx-reviewer-dashboard.png[Screenshot of the reviewer dashboard from Pretalx. Four panes are shown: 5 proposals, 6 submitters, 1 review, and 1 active reviewer.]


[[review]]
== Review proposals

A Pretalx proposal submission is typically composed of a title, abstract, track, theme(s), a brief speaker bio, and other notes.
If blind review for reviewers is enabled, speaker identities are hidden.

All reviewers can give up to four points to a session.
Each point is weighed on a different criteria, much like a rubric.
The four point categories guide each reviewer to reflect on four different aspects of the proposal:

. *Submission Quality*:
  The basics.
  Is the proposal well-written?
  Is it thoughtful?
  Does the speaker adequately describe their topic in a way that others can understand?
. *Theme Relevance*:
  All proposals link themselves to one or many of the https://communityblog.fedoraproject.org/flock-2024-cfp-until-april-21st/[six themes]:
  Accessibility (a11y), Reaching the World, Community Sustainability, Technology Innovation and Leadership, and Ecosystem Connections.
  Does the connection between proposal and theme make sense?
  Is it a good fit?
. *Probability of Success*:
  Is the speaker adequately prepared to deliver their content?
  If they will run a workshop, will it connect to the Flock audience and attendees?
  If they present a session, do they have a clear connection to Fedora?
  What is the likelihood that this session contributes to something positive for the future of Fedora?
. *Personal Appeal*:
  This is your personal point.
  Use it however you want!
  If there is a session you just _really like_ and you have a hard time explaining why, this point is for you.
  Alternatively, if there is something you really don't like, you can also downvote it (although we likely want to know why in the comments).
