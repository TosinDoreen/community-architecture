:url-accounts: https://accounts.fedoraproject.org
:url-chat: https://chat.fedoraproject.org
:url-chat-space: https://matrix.to/#/#fedora-space:fedoraproject.org
:url-commblog: https://communityblog.fedoraproject.org
:url-discussion: https://discussion.fedoraproject.org
:url-wiki: https://fedoraproject.org/wiki
:year: 2024
