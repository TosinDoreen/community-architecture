include::ROOT:partial$attributes.adoc[]

= Participate in the Fedora community

The Fedora community is large and decentralized.
You can spend several years in Fedora and still never know everything happening at once.
Even though the community is so big and spread out, there are common tips and methods you can follow to participate in Fedora.
This section is a guide to explain the different steps and phases of joining as a new contributor to Fedora, regardless of where you want to focus your contributions.

You can understand the journey of participation in these key phases:

* xref:phase0-bootstrap.adoc[Phase 0: Bootstrap]
* Phase 1: Introduce (_coming soon_)
* Phase 2: Step in (_coming soon_)
* Phase 3: Lead (_coming soon_)

See the page for each phase for more information.
Each phase is meant to be followed sequentially.
